const auth = require("../middleware/auth");
const { Comment, validateComment } = require("../models/comment.model");
const { Game } = require("../models/game.model");
const router = require("express").Router();
const {
	readFileSync,
	promises: { writeFile },
} = require("fs");

let lastCommentId = parseInt(readFileSync("lastCommentId.txt").toString());

// -----------------------------
// - Interacting With Comments -
// -----------------------------

router.post("/new", auth, async (req, res) => {
	// Validate the request body first
	const { error } = validateComment(req.body);
	if (error) return res.status(400).json({ message: error.details[0].message });

	// Find the game
	let gameId = req.body.game;
	let game = await Game.findOne({ id: gameId });
	if (!game) return res.status(400).json({ message: "Attempted to comment on unknown game" });

	let author = req.user.id;

	// TODO limit comments per user per game (10?)
	let comment = new Comment({
		game: gameId,
		author,
		content: req.body.content,
		id: ++lastCommentId,
		up: [author],
		upCount: 1,
		down: [],
	});

	game.comments++;
	await comment.save();
	await game.save();
	// sync? more like stync
	await writeFile("lastCommentId.txt", lastCommentId.toString());

	return res.status(201).json({
		message: "success",
		content: {
			author,
			content: req.body.content,
			id: comment.id,
			score: 1,
			yourVote: 1,
		}
	});
});

router.post("/vote", auth, async (req, res) => {
	// Find the comment
	let commentId = req.body.id;
	
	let comment = await Comment.findOne({ id: commentId })
	if (!comment) return res.status(400).json({ message: "Attempted to vote on an unknown comment" });
	
	let voter = req.user.id;
	let type = "";
	let other = "";

	switch (req.body.vote) {
		case 1:
			type = "up";
			other = "down";
			break;
		case -1:
			type = "down";
			other = "up";
			break;
		default:
			// If you're using the API & want to retract votes, simply re-use the
			// current vote. Example:
			// Currently: likes comment
			// To un-like, submit 1
			return res.status(400).json({ message: "Unknown vote score (expected 1 or -1)" });
	}

	if (comment[type].includes(voter)) {
		// Retract a vote
		comment[type] = comment[type].filter(id => id !== voter);
		comment[`${type}Count`]--;
	} else if (comment[other].includes(voter)) {
		// Swap the vote
		comment[other] = comment[other].filter(id => id !== voter);
		comment[`${other}Count`]--;

		comment[`${type}Count`]++;
		comment[type].push(voter);
	} else {
		// First time voting on comment
		comment[`${type}Count`]++;
		comment[type].push(voter);
	}

	await comment.save();

	return res.status(200).json({
		message: "success",
	});
});

router.post("/reply", auth, async (req, res) => {
	// Find the comment
	let commentId = req.body.id;

	let comment = await Comment.findOne({ id: commentId });
	if (!comment)
		return res
			.status(400)
			.json({ message: "Attempted to reply to an unknown comment" });

	let game = await Game.findOne({ id: comment.game });
	if (game.author !== req.user.id) return res.status(401).json({ message: "You need to be the game author to do that!" });

	let reply = req.body.content;
	if (reply === undefined || typeof reply !== "string" || reply === "" || reply.length > 8192)
		return res.status(400).json({ message: "Invalid reply (too long or nonexistent)" });

	comment.devResponse = reply;
	await comment.save();

	return res.status(200).json({
		message: "success",
	});
})

// ---------------------
// - Fetching Comments -
// ---------------------

router.get("/fetch/new", auth.optional, async (req, res) => {
	// Game id
	const { id } = req.query;

	let comments = await Comment.find({ game: id })
		.sort({ createdAt: -1 })
		.limit(5);

	// If the user is authenticated, and they have
	// voted on this comment in the past, let them 
	// know what their vote on the comment was
	if (req.user) {
		comments = comments.map(c => ({
			author: c.author,
			id: c.id,
			content: c.content,
			devResponse: c.devResponse,
			score: c.upCount - c.downCount,
			// Wow this code sucks
			yourVote: c.up.includes(req.user.id)
				? 1
				: c.down.includes(req.user.id)
				? -1
				: 0,
		}));
	} else {
		comments = comments.map(c => ({
			author: c.author,
			id: c.id,
			content: c.content,
			devResponse: c.devResponse,
			score: c.upCount - c.downCount,
		}));
	}
	return res.status(200).json(comments);
});

module.exports = router;
