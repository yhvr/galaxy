const auth = require("../middleware/auth");
const { Rating } = require("../models/rating.model");
const { Game } = require("../models/game.model");
const router = require("express").Router();

router.post("/", auth, async (req, res) => {
	// Validate the request body first
	if (req.body.rating < 1 || req.body.rating > 5)
		res.status(400).json({ message: "Invalid rating" });

	// Find the game
	let gameId = parseInt(req.body.id);

	let game = await Game.findOne({ id: gameId });
	if (!game) res.status(400).json({ message: "Attempted to rate unknown game" });

	let author = req.user.id;

	// Check if rating already exists
	let rating = await Rating.findOne({
		game: gameId,
		author,
	});

	let stars = Math.round(req.body.rating);

	if (rating) {
		// Update rating
		game.ratingValue -= rating.rating;
		rating.rating = stars;
	} else {
		// Create rating
		rating = new Rating({
			game: gameId,
			author,
			rating: stars,
		});

		game.ratingCount++;
	}

	game.ratingValue += stars;
	game.ratingAvg = game.ratingValue / game.ratingCount;

	// "The usual trick for this is to add a 1 star and a 5 star review.
	// It'll have more impact on scores with fewer "real" votes, bringing
	// them closer to 3.0, but will basically not touch sufficiently rated
	// scores at all" -ThePaperPilot
	game.sortRating = (game.ratingValue + 6) / (game.ratingCount + 2);

	// game.sortRating = game.ratingAvg + Math.log10(Math.min(game.ratingCount, 100))

	await rating.save();
	await game.save();

	return res
		.status(200)
		.json({
			message: "success",
			rating: game.ratingAvg,
			count: game.ratingCount,
		});
});

module.exports = router;
