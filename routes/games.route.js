const auth = require("../middleware/auth");
const { Game, validate, validateEdit } = require("../models/game.model");
const router = require("express").Router();
const {
	readFileSync,
	promises: { writeFile },
} = require("fs");
const sharp = require("sharp");
const fileUpload = require("express-fileupload");
const { validTagList } = require("../src/tags");

let lastGameId = parseInt(readFileSync("lastGameId.txt").toString());

// ------------------
// - Fetching Games -
// ------------------

router.post("/new", auth, async (req, res) => {
	// Validate the request body first
	const { error } = validate(req.body);
	if (error) return res.status(400).json({ message: error.details[0].message });

	// Find an existing game
	let game = await Game.findOne({ name: req.body.name });
	if (game)
		return res.status(400).json({ message: "A game already exists with that name." });

	game = await Game.findOne({ link: req.body.link });
	if (game)
		return res.status(400).json({ message: "A game already exists with that link." });

	game = new Game({
		name: req.body.name,
		description: req.body.description || "No description provided.",
		link: req.body.link,
		id: ++lastGameId,
		author: req.user.id,

		unlisted: req.body.private || req.body.unlisted,
		// Dirty AF boolean conversion
		private: !!req.body.private,
	});
	await game.save();

	await writeFile("lastGameId.txt", lastGameId.toString());

	// Yes, I know I'm not awaiting this. So what?
	updateThumbs(sharp("static/unknown-game.webp"), lastGameId);

	return res.status(201).json({ message: "Thanks for submitting!", id: game.id });
});

let sortTypes = { rating: "sortRating", update: "lastUpdate" };
function parseSearch(query) {
	return {
		term: typeof query.term === "string" ? query.term : "",
		sort: sortTypes[query.sort] ?? "sortRating",
		tags: validTagList(query.tags) ? query.tags : [],
		dir: query.reverse ? 1 : -1,
	};
}

router.get("/search", async (req, res) => {
	const { term, tags, sort, dir } = parseSearch(req.query);
	if (term.length >= 100) return res.status(400).json({ message: "Shorten your search term" })

	const pipeline = [
		{
			$project: {
				_id: 0,
				id: 1,
				author: 1,
				name: 1,
				ratingAvg: 1,
				description: 1,
				shared: { $setIntersection: [tags, "$tags"] },
			},
		},
		{
			$match: tags.length === 0 ? ({}) : ({
				[`shared.${tags.length - 1}`]: { $exists: true },
			}),
		},
		{
			$sort: { [sort]: dir },
		},
	];
	if (term !== "") pipeline.unshift({
		$match: {
			$text: { $search: term },
		},
	});

	const games = await Game.aggregate(pipeline).limit(50);

	return res.status(200).json(
		games.map(game => ({
			name: game.name,
			description: game.description,
			// link: game.link,
			rating: game.ratingAvg,
			sortRating: game.sortRating,
			author: game.author,
			createdAt: game.createdAt,
			id: game.id,
			tags: game.tags,
		}))
	);
});

router.get("/query", async (req, res) => {
	console.log(req.query)
});

router.get("/info", async (req, res) => {
	if (!req.query.id) return res.status(400).json({ message: "id not specified" })
	const { id } = req.query;

	const game = await Game.findOne({ id });
	if (game === null) return res.status(404).json({ message: "Game not found" });

	return res.status(200).json({
		name: game.name,
		author: game.author,
		type: game.type,
		link: game.link,
		description: game.description,
		rating: game.ratingAvg,
		ratingCount: game.ratingCount,
		tags: game.tags,
	});
});

const MINIMUM_TAG_OVERLAP = 1
const MINIMUM_TAG_QUERY = `shared.${MINIMUM_TAG_OVERLAP - 1}`;
router.get("/similar", async (req, res) => {
	if (!req.query.id) return res.status(400).json({ message: "id not specified" })
	const { id } = req.query;

	const game = await Game.findOne({ id });
	if (game === null) return res.status(404).json({ message: "Game not found" });

	let out = await Game.aggregate([
		{
			$project: { _id: 0, id: 1, author: 1, name: 1, ratingAvg: 1, shared: { $setIntersection: [ game.tags, "$tags" ] } }
		},
		{
			// This is stupid as fuck but it works.
			// It basically makes sure `shared` has `n` elements by checking if `shared[n-1]` exists
			// I tried doing $match:{shared:{$size:{$gte:2}}} but it didn't work
			// Update (the next day): apparently smiley found an SO post that says this is a good way to do it.
			$match: { [MINIMUM_TAG_QUERY]: { $exists: true }, id: { $ne: game.id } },
		},
		{
			$sort: { ratingAvg: -1 }
		}
	]).limit(6);

	return res.status(200).json(out);
});

// -----------------
// - Editing Games -
// -----------------

router.post("/edit", auth, async (req, res) => {
	// Validate the request body first
	const { error } = validateEdit(req.body.new);
	if (error)
		return res.status(400).json({ message: error.details[0].message });

	// Find an existing game
	const game = await Game.findOne({ id: req.body.id });
	if (!game) return res.status(400).json({ message: "Game not found." });

	if (game.author !== req.user.id)
		return res.status(401).json({ message: "You are not the author of this game" });

	if (req.body.new.name) game.name = req.body.new.name;
	if (req.body.new.description) game.description = req.body.new.description;
	if (req.body.new.link) game.link = req.body.new.link;

	await game.save();

	return res.status(201).json({ message: "Edit successful" });
});


router.post(
	"/thumb",
	fileUpload({
		limits: {
			fileSize: 8 * 1024 * 1024,
		},
	}),
	auth,
	async (req, res) => {
		let game = await Game.findOne({ id: req.body.id });
		if (!game)
			return res.status(400).json({
				message: "Game not found",
			});
		if (game.author !== req.user.id)
			return res.status(401).json({
				message: "You are not the author of this game"
			});
		if (!req.files.image)
			return res.status(400).json({
				message: "No image provided",
			});

		let file = sharp(req.files.image.data);
		await updateThumbs(file, game.id).catch(e => {
			return res
				.status(400)
				.json({
					message:
						"We couldn't figure out what type of image that was.",
				});
		});

		return res.status(200).json({ message: "Success!" });
	}
);

router.post("/tags", auth, async (req, res) => {
	// Find an existing game
	const game = await Game.findOne({ id: req.body.id });
	if (!game) return res.status(400).json({ message: "Game not found." });

	if (game.author !== req.user.id)
		return res.status(401).json({ message: "You are not the author of this game" });

	if (!validTagList(req.body.tags)) return res.status(400).json({ message: "Invalid tag list" });

	game.tags = req.body.tags;
	await game.save();

	return res.status(200).json({ message: "Tag change successful" });
})

// --------------------
// - Helper Functions -
// --------------------
// TODO merge this with the profile pic func
async function updateThumbs(sharpFile, gid) {
	let largeImg = sharpFile.webp().resize(500, 300);
	let medImg = largeImg.clone().resize(375, 225);
	let smallImg = medImg.clone().resize(250, 150);

	let largeBuffer = await largeImg.toBuffer();
	let medBuffer = await medImg.toBuffer();
	let smallBuffer = await smallImg.toBuffer();

	await writeFile(`static/img/thumb/large/${gid}.webp`, largeBuffer);
	await writeFile(`static/img/thumb/medium/${gid}.webp`, medBuffer);
	await writeFile(`static/img/thumb/small/${gid}.webp`, smallBuffer);
}

module.exports = router;
