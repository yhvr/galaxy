// const auth = require("../middleware/auth");
// const { Game } = require("../models/game.model");
const router = require("express").Router();
const { getAllTags } = require("../src/tags");

router.get("/list", (req, res) => {
	res.status(200).json(getAllTags());
});

module.exports = router;