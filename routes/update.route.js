const auth = require("../middleware/auth");
const { Update, validateUpdate } = require("../models/update.model");
const { Game } = require("../models/game.model");
const router = require("express").Router();
const {
	readFileSync,
	promises: { writeFile },
} = require("fs");

let lastUpdateId = parseInt(readFileSync("lastUpdateId.txt").toString());

router.post("/new", auth, async (req, res) => {
	// Validate the request body first
	const { error } = validateUpdate(req.body);
	if (error)
		return res.status(400).json({ message: error.details[0].message });

	// Find the game
	let gameId = req.body.game;
	let game = await Game.findOne({ id: gameId });
	if (!game)
		return res.status(400).json({
			message: "Attempted to update unknown game",
		});

	let author = req.user.id;
	if (author !== game.author)
		return res
			.status(401)
			.json({ message: "Attempted to make unauthorized update" });

	let update = new Update({
		game: gameId,
		changelog: req.body.changelog,
		version: req.body.version,
		name: req.body.name,
		id: ++lastUpdateId,
	});

	game.lastUpdate = Date.now();
	await update.save();
	await game.save();
	// sync? more like stync
	await writeFile("lastUpdateId.txt", lastUpdateId.toString());

	return res.status(201).json({
		message: "Update published! Enjoy the bump.",
	});
});

router.get("/get", async (req, res) => {
	// Game id
	const { id } = req.query;

	let update = await Update.find({ game: id })
		.sort({ createdAt: -1 })
		.limit(1);

	if (update[0])
		update = {
			id: update[0].id,
			changelog: update[0].changelog,
			version: update[0].version,
			name: update[0].name,
		};
	else update = false;
	

	return res.status(200).json(update);
});

// TODO frontend for this
router.get("/get/full", async (req, res) => {
	// Game id
	const { id } = req.query;

	let updates = await Update.find({ game: id }).sort({ createdAt: -1 });

	updates = updates.map(u => ({
		id: u.id,
		changelog: u.changelog,
		version: u.version,
		name: u.name,
	}));

	return res.status(200).json(updates);
});

module.exports = router;
