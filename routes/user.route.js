const auth = require("../middleware/auth");
const bcrypt = require("bcrypt");
const { User, validate, generateAuthToken } = require("../models/user.model");
const router = require("express").Router();
const sharp = require("sharp");
const rateLimit = require("express-rate-limit");
const {
	readFileSync,
	promises: { writeFile },
} = require("fs");
const fileUpload = require("express-fileupload");
const { Game } = require("../models/game.model");

const loginLimiter = rateLimit({
	windowMs: 5 * 60 * 1000,
	max: 25,
});
const signupLimiter = rateLimit({
	windowMs: 60 * 60 * 1000,
	max: 5,
});

router.get("/current", auth, async (req, res) => {
	const user = await User.findById(req.user._id).select("-password");
	res.status(200).json(user);
});

router.post("/login", loginLimiter, async (req, res) => {
	// Find an existing user
	const user = await User.findOne({ email: req.body.email });
	if (!user) return res.status(400).json({ message: "User not registered." });

	const equal = await bcrypt.compare(req.body.password, user.password);
	if (!equal) return res.status(400).json({ message: "Wrong password." });

	const token = await generateAuthToken(user);
	res.cookie("token", token, {
		// 1 week
		maxAge: 1000 * 60 * 60 * 24 * 7,
		httpOnly: true,
		sameSite: "strict",
		// TODO (after https)
		// secure: true,
	}).status(200).send({
		name: user.name,
		email: user.email,
		id: user.id,
	});
});

let lastUserId = parseInt(readFileSync("lastUserId.txt").toString());

router.post("/signup", signupLimiter, async (req, res) => {
	// Validate the request body first
	const { error } = validate(req.body);
	if (error)
		return res.status(400).json({ message: error.details[0].message });

	// Find an existing user
	let user = await User.findOne({ email: req.body.email });
	if (user)
		return res
			.status(400)
			.json({ message: "An account already exists with that email." });
	user = await User.findOne({ name: req.body.name });
	if (user)
		return res
			.status(400)
			.json({ message: "That username is taken. Try something else." });

	user = new User({
		name: req.body.name,
		password: req.body.password,
		email: req.body.email,
		id: ++lastUserId,
	});
	user.password = await bcrypt.hash(user.password, 12);
	await user.save();

	await writeFile("lastUserId.txt", lastUserId.toString());

	// Yes, I know I'm not awaiting this. So what?
	updatePfps(sharp("static/unknown-user.webp"), lastUserId);

	const token = await generateAuthToken(user);
	res.cookie("token", token, {
		// 1 week
		maxAge: 1000 * 60 * 60 * 24 * 7,
		httpOnly: true,
		sameSite: "strict",
		// TODO (after https)
		// secure: true,
	})
		.status(201)
		.json({
			name: user.name,
			email: user.email,
			id: user.id,	
		});
});

router.get("/logout", async (req, res) => {
	res.clearCookie("token").status(200).json({ message: "Removed token cookie" });
});

router.get("/whois", async (req, res) => {
	let user = await User.findOne({ id: req.query.id });

	if (!user) return res.status(400).send({ message: "Unknown user" });

	res.status(200).send({
		name: user.name,
	});
});

router.get("/whois/full", auth.optional, async (req, res) => {
	let user = await User.findOne({ id: req.query.id });

	if (!user) return res.status(400).json({ message: "Unknown user" });

	let games = await Game.find({ author: req.query.id, unlisted: false });
	let minimizedGames = games.map(game => ({
		name: game.name,
		description: game.description,
		id: game.id,
		ratings: game.ratingCount,
		ratingTotal: game.ratingValue,
	}));

	res.status(200).json({
		user: {
			name: user.name,
			id: user.id,
			bio: user.bio,
			flair: user.equippedFlair
		},
		games: minimizedGames
	})
});

// ----------------------
// - Modifying Yourself -
// ----------------------
router.post(
	"/pfp",
	fileUpload({
		limits: {
			fileSize: 8 * 1024 * 1024,
		},
	}),
	auth,
	async (req, res) => {
		let user = await User.findOne({ id: req.user.id });
		if (!user)
			return res.status(400).json({
				message: "Who are you? How did you get this message?",
			});
		if (!req.files.image)
			return res.status(400).json({
				message: "No image provided",
			});

		let file = sharp(req.files.image.data);
		await updatePfps(file, req.user.id).catch(e => {
			return res.status(400).json({ message: "We couldn't figure out what type of image that was." })
		})

		return res.status(200).json({ message: "Success!" });
	}
);

router.post("/bio", auth, async (req, res) => {
	let user = await User.findOne({ id: req.user.id });
	if (!user) res.status(400).send({ message: "WTF?" });

	if (req.body.bio === undefined) return res.status(400).send({ message: "No bio provided." });
	let bio = req.body.bio.toString();
	if (bio.length > 1000) return res.status(400).send({ message: "Bios are limited to 1,000 characters." });

	user.bio = bio;
	await user.save();

	res.status(200).send({ message: "Bio updated!" });
});

// --------------------
// - Helper Functions -
// --------------------
async function updatePfps(sharpFile, uid) {
	let largeImg = sharpFile.webp().resize(256, 256);
	let medImg = largeImg.clone().resize(64);
	let smallImg = medImg.clone().resize(16);

	let largeBuffer = await largeImg.toBuffer();
	let medBuffer = await medImg.toBuffer();
	let smallBuffer = await smallImg.toBuffer();

	await writeFile(`static/img/pfp/large/${uid}.webp`, largeBuffer);
	await writeFile(`static/img/pfp/medium/${uid}.webp`, medBuffer);
	await writeFile(`static/img/pfp/small/${uid}.webp`, smallBuffer);
}

module.exports = router;
