const Joi = require("joi");
const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema(
	{
		game: Number,
		author: Number,
		id: Number,
		content: {
			type: String,
			minLength: 1,
			maxLegth: 10_000,
			required: true,
		},
		devResponse: {
			type: String,
			minLength: 1,
			maxLength: 8192,
			required: false,
		},
		up: Array,
		down: Array,
		upCount: {
			type: Number,
			default: 0,
		},
		downCount: {
			type: Number,
			default: 0,
		},
	},
	{
		timestamps: true,
	}
);

const Comment = mongoose.model("Comments", CommentSchema);

// ~~TODO do i really need to be using Joi for this?
// It's probably easiest
// Who cares
const content = Joi.string().min(1).max(10_000).required();
const id = Joi.number().greater(0).integer().required();

const schema = Joi.object({
	content,
	game: id,
});

function validateComment(comment) {
	return schema.validate(comment);
}

module.exports = { Comment, validateComment };