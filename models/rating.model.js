const Joi = require("joi");
const mongoose = require("mongoose");

const RatingSchema = new mongoose.Schema(
	{
		game: Number,
		author: Number,
		rating: {
			type: Number,
			min: 1,
			max: 5,
		}
	},
	{
		timestamps: true,
	}
);

const Rating = mongoose.model("Rates", RatingSchema);

module.exports = { Rating };