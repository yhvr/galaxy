const Joi = require("joi");
const mongoose = require("mongoose");

const UpdateSchema = new mongoose.Schema(
	{
		game: Number,
		id: Number,
		changelog: {
			type: String,
			minLength: 1,
			maxLegth: 8_196,
			required: true,
		},
		name: {
			type: String,
			required: false,
		},
		version: {
			type: String,
			required: false,
		},
	},
	{
		timestamps: true,
	}
);

const Update = mongoose.model("Updates", UpdateSchema);

// ~~TODO do i really need to be using Joi for this?
// It's probably easiest
// Who cares
const changelog = Joi.string().min(1).max(8_196).required();
const version = Joi.string().max(32).optional().allow("");
const name = Joi.string().max(128).optional().allow("");
const id = Joi.number().greater(0).integer().required();

const schema = Joi.object({
	changelog,
	version,
	name,
	game: id,
});

function validateUpdate(update) {
	console.log(update);
	return schema.validate(update);
}

module.exports = { Update, validateUpdate };
