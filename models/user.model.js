const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const mongoose = require("mongoose");

// Simple schema
const UserSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		minlength: 3,
		maxlength: 50,
	},
	email: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 255,
		unique: true,
	},
	emailVerified: {
		type: Boolean,
		default: false
	},
	password: {
		type: String,
		required: true,
	},
	id: Number,

	bio: {
		type: String,
		maxLength: 1000,
		default: "",
	},
	
	flairs: {
		type: [String],
		default: ["none"]
	},
	equippedFlair: {
		type: String,
		default: "none"
	},

	// OK, here's how this works.
	// since you can't exactly "revoke" JWTs, i store
	// a number in them to make sure that when someone logged out all instances,
	// or changes their password, every single instance gets logged out.
	// Well, technically they still have the cookie.
	// However, the site does not accept it because the state in the JWT is lower then that in the userid
	sessionState: {
		type: Number,
		default: 0,
	}
});

module.exports.generateAuthToken = async (user) => {
	// Get the private key from the config file -> environment variable
	const token = jwt.sign(
		{ id: user.id, state: user.sessionState },
		config.get("myprivatekey"),
		// TODO { expiresIn: ??? }
	);
	return token;
};

const User = mongoose.model("User", UserSchema);

// Function to validate user
const schema = Joi.object({
	name: Joi.string().min(3).max(50).required(),
	email: Joi.string().min(5).max(255).required().email(),
	password: Joi.string().min(3).max(255).required(),
});

function validateUser(user) {
	return schema.validate(user);
}

exports.User = User;
exports.validate = validateUser;
