const Joi = require("joi");
const mongoose = require("mongoose");
const { allValidTags, getAllTags } = require("../src/tags");

function validateTags(tags) {
	if (tags.length > 3) return false;
	return allValidTags(tags);
}

const GameSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
			minlength: 3,
			maxlength: 128,
			unique: true,
		},
		type: {
			type: String,
			enum: ["iframe"],
			default: "iframe",
		},
		link: {
			type: String,
			required: true,
			minlength: 3,
			maxlength: 128,
			unique: true,
		},
		description: {
			type: String,
			maxlength: 8192,
		},
		author: Number,
		id: Number,

		// Ratings:
		// ratingCount: the # of ratings
		// ratingValue: the total # of stars
		// ratingAvg: (value / count)
		// sortRating: (avg) + log10(count)
		ratingCount: {
			type: Number,
			default: 0,
		},
		ratingAvg: {
			type: Number,
			default: 0,
		},
		ratingValue: {
			type: Number,
			default: 0,
		},
		sortRating: {
			type: Number,
			default: 0,
		},

		comments: {
			type: Number,
			default: 0,
		},

		// Game tags
		tags: {
			type: [String],
			default: [],
			validate: [validateTags, "Too many (or invalid) tags"],
		},

		// TODO this is unused
		plays: {
			type: Number,
			default: 0,
		},

		// Visibility
		unlisted: { type: Boolean, default: false },
		private: { type: Boolean, default: false },

		// Chat
		chatEnabled: { type: Boolean, default: true },
		pinnedMessages: [String],
		webhookURL: {
			type: String,
			default: "",
		},

		// Updates
		lastUpdate: { type: Date, default: Date.now },
	},
	{
		timestamps: true,
	}
).index({
	name: "text",
	description: "text",
});

const Game = mongoose.model("Game", GameSchema);

const name = Joi.string().min(3).max(128);
const type = Joi.string().default("iframe").valid("iframe");
const link = Joi.string().min(3).max(256).uri();
const description = Joi.string().max(8192);
// const tag = Joi.string().valid(...getAllTags());
// const tags = Joi.array().items(tag).max(3);
const optBool = Joi.boolean().optional();

const schema = Joi.object({
	name,
	type,
	link,
	description: description.optional(),
	unlisted: optBool,
	private: optBool,
});

function validateGame(game) {
	return schema.validate(game);
}

const editSchema = Joi.object({
	name,
	link: link.optional(),
	description: description.optional(),
}).min(1);

function validateEdit(game) {
	return editSchema.validate(game);
}

exports.Game = Game;
exports.validate = validateGame;
exports.validateEdit = validateEdit;
