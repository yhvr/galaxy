const config = require("config");
const mongoose = require("mongoose");
const usersRoute = require("./routes/user.route");
const gameRoute = require("./routes/games.route");
const ratingRoute = require("./routes/rating.route");
const commentRoute = require("./routes/comment.route");
const tagRoute = require("./routes/tags.route");
const updateRoute = require("./routes/update.route");
const wss = require("./src/ws");
const express = require("express");
require("express-async-errors");
const app = express();

// Use config module to get the privatekey, if no private key set, end the application
if (!config.get("myprivatekey")) {
	console.error("FATAL ERROR: myprivatekey is not defined.");
	process.exit(1);
}

mongoose
	.connect("mongodb://0.0.0.0/galaxy")
	// .then(() => console.log("Connected to MongoDB..."))
	// .catch(() => console.error("Could not connect to MongoDB..."));

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}))
app.use(require("cookie-parser")());

// API routes
app.use("/api/users", usersRoute);
app.use("/api/games", gameRoute);
app.use("/api/rate", ratingRoute);
app.use("/api/comments", commentRoute);
app.use("/api/tags", tagRoute);
app.use("/api/updates", updateRoute);

// Static routes
app.use("/", express.static("web"));
app.use("/static", express.static("static"));
app.use("/play/*", express.static("web/play"));
app.use("/user/*", express.static("web/user"));
app.use("/edit/*", express.static("web/edit"));

app.get("/sex", (_, res) => res.status(418).send("bro what"))

const port = process.env.PORT || 3000;
const server = app.listen(port);

// Chat (funky websocket thing)
server.on("upgrade", (req, socket, head) => {
	wss.handleUpgrade(req, socket, head, socket => {
		wss.emit("connection", socket, req);
	});
});