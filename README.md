# THIS REPO IS NO LONGER IN USE.
# Check out [galaxy-astro](https://gitlab.com/yhvr/galaxy-astro)

# galaxy

**the place for incremental games.**

a website for uploading games from the "incremental" genre. made with node, express, and mongoose.

devs: upload your incremental games, get more exposure, and get more feedback.

players: find new incrementals, give feedback, and talk with other fans. no need to join a dozen discord servers.

## setup

1. clone this repository to your computer, and `cd` into it
2. `npm install`
3. set up a mongodb database on your local machine. default port, no credentials (for now)
4. `node .`
5. navigate to `localhost:3000` in your browser

## why make this?

* **experience.** who all can say they've made a site as complex as this? it looks kinda good on a resume.
* **developer experience.** keep new people consistently discovering your games. a post on r/incremental_games fades into obscurity, a game on galaxy doesn't.
* **user experience.** on discord, you can only join 100 servers max. on galaxy, you can chat with people playing the same game, without having to get closer to the discord server cap.
	* you can also leave comments and rate the game on a 1-5 scale. you can't really do that anymore, since kongregate shut down ;)

## directory structure

* `/middleware` - express middleware. as of writing this, the only middleware is authentication.
* `/models` - mongoose schemas, and some user input validation functions (using joi).
* `/routes` - API routes
* `/src` - mostly just helper functions, and stuff that didn't fit anywhere else.
* `/static` - ironically, *not* static things. i know, the name isn't the best. it stores profile pictures and game thumbnails, in 3 different sizes each.
* `/web` - pages on the website.
* `/web/js` - javascript files that the browser loads.
* `/web/*/index.html` - glob directories--ie, if you go to `/play/1/` or `/play/123`, you'll still just load `/web/play/index.html`. it's not pretty, but it gets the job done :)
* **bonus item:** `/grant*.md` - files pertaining to my conversations with Grant Gryczan, the owner of MSPFA, a website with a very similar structure, but for an entirely different niche. 

## special thanks

galaxy is only this good--or exists at all--thanks to all of you.
* Kongregate
* r/incremental_games
* smileyrandom
* ThePaperPilot
* Everyone else on my Discord server who gave feedback and voted on polls
* Grant Gryczan
