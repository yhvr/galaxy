const jwt = require("jsonwebtoken");
const config = require("config");
const { User } = require("../models/user.model");
const { default: asyncMiddleware } = require("middleware-async");


// ~~TODO: store IP in JWT and verify IP in auth
// ^^ actually--should i be doing this?

module.exports = asyncMiddleware(async (req, res, next) => {
	const token = req.cookies.token || req.headers.authorization;
	if (!token)
		return res.status(401).json({ message: "You need an account to do that!", _LOGOUT: true });
	try {
		const decoded = jwt.verify(token, config.get("myprivatekey"));
		req.user = decoded;

		let user = await User.findOne({ id: decoded.id });
		if (user.sessionState !== decoded.state) return res.status(401).clearCookie("token").json({ message: "Please log in again.", _LOGOUT: true });
		
		delete user.password;
		delete user.email;
		req.user.db = user;

		next();
	} catch (e) {
		res.status(400).json({ message: "Your token is invalid. Try logging out and logging in again.", _LOGOUT: true });
	}
});

module.exports.optional = asyncMiddleware(async (req, res, next) => {
	const token = req.cookies.token || req.headers.authorization;

	if (!token) return next();
	try {
		const decoded = jwt.verify(token, config.get("myprivatekey"));
		req.user = decoded;

		let user = await User.findOne({ id: decoded.id });
		if (!user) return next();
		if (user.sessionState !== decoded.state) return res.status(401).clearCookie("token").json({ message: "Please log in again.", _LOGOUT: true });

		delete user.password;
		delete user.email;
		req.user.db = user;

		next();
	} catch (e) {
		res.status(400).json({ message: "Your token is invalid. Try logging out and logging in again.", _LOGOUT: true });
	}
});

module.exports.standalone = async (token) => {
	if (token) {
		try {
			let decoded = jwt.verify(token, config.get("myprivatekey"));
			let user = await User.findOne({ id: decoded.id });
			if (user.sessionState !== decoded.state) return false;

			delete user.password;
			delete user.email;

			return user;
		} catch (e) {
			// ... just return `false` later on
		}
	}

	return false;
}
