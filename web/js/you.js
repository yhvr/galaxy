let vue = {
	bio: "",
	pfpState: 0,
};

// Fetch bio
async function postinit() {
	let full = await galaxy.json("/api/users/whois/full?id=" + app.user.id);

	app.bio = full.user.bio;
}

galaxy.init();

app.pfpForm = galaxy.form("pfpForm", ["image"], res => {
	app.pfpState++;
	if (res._resCode === 200) galaxy.alert.success("Profile picture changed!");
}, true);

app.bioForm = galaxy.form("bioForm", ["bio"], res => {
	if (res._resCode === 200) galaxy.alert.success("Bio updated!");
});

function logout() {
	localStorage.removeItem("user");
	galaxy.json("/api/users/logout");

	galaxy.alert.success("Logged out. Redirecting...");

	setTimeout(() => {
		window.location.pathname = "";
	}, 1000);
}
