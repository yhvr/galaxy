const components = {
	"game-list-vert": {
		props: ["games", "name"],
		template: `
			<div class="vert-game-list">
				<p class="vert-game-title">{{ name }}</p>
				<div v-for="game in games">
					<game :game="game"></game>
				</div>
			</div>
		`
	},
	game: {
		props: ["game"],
		template: `
			<div class="game">
				<a class="masked-link" :href="'play/' + game.id">
					<small-thumb :id="game.id"></small-thumb>
					<p class="game-name">{{ game.name }}</p>
				</a>
				<span class="game-more">{{ getUser(game.author).name }}</span>
				<span class="game-more floatright">{{ game.rating.toFixed(2) }}/5.00</span>
			</div>
		`,
		data() { return {
			getUser(id) { return app.users[id] }
		} }
	}
}

galaxy.json("/api/games/search")
	.then(data => {
		app.loading = false;
		app.data.games = data;

		data.forEach(game => galaxy.loadUser(game.author))

		app.data.g1 = shuffle(data).slice(0, 3)
		app.data.g2 = shuffle(data).slice(0, 3)
		app.data.g3 = shuffle(data).slice(0, 3)
	});


function shuffle(array) {
	let currentIndex = array.length,
		randomIndex;

	// While there remain elements to shuffle.
	while (currentIndex != 0) {
		// Pick a remaining element.
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex--;

		// And swap it with the current element.
		[array[currentIndex], array[randomIndex]] = [
			array[randomIndex],
			array[currentIndex],
		];
	}

	return array;
}
