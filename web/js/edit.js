const vue = {
	updateMain,
	publishUpdate,
	updateTags,
	applyTag,
	removeTag,
	thumbState: 0,
	data: {
		game: {},
		update: {}
	},
	inactiveTags: [],
}

// Probably an awful way to do this LOL
let gameId = parseInt(window.location.pathname.split("/")[2]);
loadEdit(gameId);

async function loadEdit(id) {
	let out = await galaxy.json(
		`/api/games/info?id=${id}`
	);

	app.data = {
		game: out,
		id,
		update: {
			name: "",
			number: "",
			changelog: "",
		},
	};

	if (out.author !== user.id) window.location.pathname = "";

	app.menu = "loaded";

	let tags = await galaxy.json("/api/tags/list");
	app.inactiveTags = tags.filter(tag => !out.tags.includes(tag));
}

async function updateMain() {
	let out = await galaxy.post("/api/games/edit", {
		id: gameId,
		new: {
			name: app.data.game.name,
			description: app.data.game.description,
			link: app.data.game.link,
		}
	});

	console.log(app.data);

	if (out._resCode === 201) {
		galaxy.alert.success(out.message);
	}

	console.log(out);
}

async function publishUpdate() {
	let out = await galaxy.post("/api/updates/new", {
		game: gameId,
		changelog: app.data.update.changelog,
		version: app.data.update.number,
		name: app.data.update.name,
	});

	console.log(out);
	
	if (out._resCode === 201) {
		galaxy.alert.success(out.message);
	}
}

function applyTag(tag) {
	if (app.data.game.tags.length >= 3) return false;

	app.inactiveTags = app.inactiveTags.filter(t => t !== tag);
	app.data.game.tags.push(tag);
}

function removeTag(tag) {
	app.data.game.tags = app.data.game.tags.filter(t => t !== tag);
	app.inactiveTags.push(tag);
}	

async function updateTags() {
	let res = await galaxy.post("/api/games/tags", { id: gameId, tags: app.data.game.tags });

	if (res._resCode === 200) galaxy.alert.success(res.message)
}

galaxy.init("loading");

app.thumbForm = galaxy.form(
	"thumbForm",
	["image", "id"],
	res => {
		app.thumbState++;
		if (res._resCode === 200)
			galaxy.alert.success("Game thumbnail changed!");
	},
	true
);