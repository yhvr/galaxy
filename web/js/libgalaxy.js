// oooo   o8o   .o8                            oooo
// `888   `"'  "888                            `888
//  888  oooo   888oooo.   .oooooooo  .oooo.    888   .oooo.   oooo    ooo oooo    ooo
//  888  `888   d88' `88b 888' `88b  `P  )88b   888  `P  )88b   `88b..8P'   `88.  .8'
//  888   888   888   888 888   888   .oP"888   888   .oP"888     Y888'      `88..8'
//  888   888   888   888 `88bod8P'  d8(  888   888  d8(  888   .o8"'88b      `888'
// o888o o888o  `Y8bod8P' `8oooooo.  `Y888""8o o888o `Y888""8o o88'   888o     .8'
//                        d"     YD                                        .o..P'
//                        "Y88888P'                                        `Y8P'
//
// A set of util functions that every page uses in some capacity.

let app = {};
let user = localStorage.user ? JSON.parse(localStorage.user) : false;

const galaxy = {
	init(menu = "unknown") {
		if (typeof vue !== "undefined") galaxy.loadVue(menu, vue);
		else galaxy.loadVue(menu, {});
		if (typeof postinit !== "undefined") postinit();
	},

	loadVue(menu, vueData) {
		let tempApp = Vue.createApp({
			data() {
				return {
					loading: true,
					menu,
					data: {},
					user,
					// Usercache
					users: {},

					...vueData,
				};
			},
			methods: {},
		})

		tempApp.component("top-bar", {
			props: [],
			template: `
				<div id="top">
					<a id="galaxy-top-text" href="/">
						Galaxy Beta
					</a>

					|

					<a class="masked-link" href="/search.html">
						Search
					</a>

					<a v-if="user" :href="'/user/' + user.id" class="masked-link floatright">
						<img :src="'/static/img/pfp/small/' + user.id + '.webp'" />
						{{ user.name }}
					</a>
					<span class="floatright" v-else>
						<a href="/signup.html" target="_blank">No account? Sign up!</a>
						Log in:
						<input id="loginEmail" type="email" placeholder="Email" />
						<input id="loginPassword" type="password" placeholder="Password" />
						<button @click="attemptLogin">Log in</button>
					</span>
				</div>
			`,
			data() {
				return { user };
			},
			methods: {
				async attemptLogin() {
					let email = document.getElementById("loginEmail").value;
					let password =
						document.getElementById("loginPassword").value;

					if (email && password) {
						let res = await galaxy.post("/api/users/login", {
							email,
							password,
						});
						if (res.name) {
							galaxy.alert.success(`Welcome back, ${res.name}!`);
							user = {
								name: res.name,
								email: res.email,
								id: res.id,
							};
							localStorage.user = JSON.stringify(user);
							setTimeout(() => window.location.reload(), 1000);
						}
					}
				},
			},
		});

		if (typeof components !== "undefined") {
			for (const key in components) tempApp.component(key, components[key])
		}

		tempApp.component("small-pfp", {
			props: ["id"],
			template: `<img :src="'/static/img/pfp/small/' + id + '.webp'">`,
		});
		tempApp.component("medium-pfp", {
			props: ["id"],
			template: `<img :src="'/static/img/pfp/medium/' + id + '.webp'">`,
		});
		tempApp.component("large-pfp", {
			props: ["id"],
			template: `<img :src="'/static/img/pfp/large/' + id + '.webp'">`,
		});

		tempApp.component("small-thumb", {
			props: ["id"],
			template: `<img :src="'/static/img/thumb/small/' + id + '.webp'">`,
		});
		tempApp.component("medium-thumb", {
			props: ["id"],
			template: `<img :src="'/static/img/thumb/medium/' + id + '.webp'">`,
		});
		tempApp.component("large-thumb", {
			props: ["id"],
			template: `<img :src="'/static/img/thumb/large/' + id + '.webp'">`,
		});

		app = tempApp.mount("#app");
	},

	//  _     _   _
	// | |__ | |_| |_ _ __
	// | '_ \| __| __| '_ \
	// | | | | |_| |_| |_) |
	// |_| |_|\__|\__| .__/
	//               |_|
	// `fetch` wrapper functions
	// prevents a lot of redundant code

	async json(url) {
		let req = await fetch(url);
		let out = await req.json();
		if (out._LOGOUT) galaxy.logout(true);
		return out;
	},

	async post(url, body, { method = "json", submitAsJson = true } = {}) {
		let headers = {
			Accept: "application/json",
		};
		if (submitAsJson) headers["Content-Type"] = "application/json";

		let req = await fetch(url, {
			method: "POST",
			headers,
			body: submitAsJson ? JSON.stringify(body) : body,
		});

		if (method) {
			let out = await req[method]();
			if (req.status >= 400 && req.status < 500 && out.message)
				galaxy.alert.error(out.message);
			out._resCode = req.status;

			if (out._LOGOUT) galaxy.logout(true);

			return out;
		}
		return req;
	},

	// 	  __
	//  / _| ___  _ __ _ __ ___  ___
	// | |_ / _ \| '__| '_ ` _ \/ __|
	// |  _| (_) | |  | | | | | \__ \
	// |_|  \___/|_|  |_| |_| |_|___/

	form(id, props, out, forceMultipart = false) {
		return (event) => {
			event.preventDefault();
			event.stopPropagation();
			
			const form = document.getElementById(id);

			const formData = new FormData();
			const jsonForm = {};

			
			props.forEach(prop => {
				if (form[prop].files) formData.append(prop, form[prop].files[0]);
				else formData.append(prop, form[prop].value);
				
				jsonForm[prop] = form[prop].value;
			});
 
			galaxy
				.post(form.action, forceMultipart ? formData : jsonForm, {
					submitAsJson: !forceMultipart,
				})
				.then(out)
				.catch(e => {
					console.log(e);
					galaxy.alert.error(
						`There was an error with your input. ${e}`
					);
				});
		};
	},

	//  _ __   ___  _ __  _   _ _ __  ___
	// | '_ \ / _ \| '_ \| | | | '_ \/ __|
	// | |_) | (_) | |_) | |_| | |_) \__ \
	// | .__/ \___/| .__/ \__,_| .__/|___/
	// |_|         |_|         |_|

	alert: {
		error: createGalaxyAlert("alert-red"),
		success: createGalaxyAlert("alert-green"),
		info: createGalaxyAlert("alert-blue"),
	},

	// 	                                  _
	//  _   _ ___  ___ _ __ ___ __ _  ___| |__   ___
	// | | | / __|/ _ \ '__/ __/ _` |/ __| '_ \ / _ \
	// | |_| \__ \  __/ | | (_| (_| | (__| | | |  __/
	//  \__,_|___/\___|_|  \___\__,_|\___|_| |_|\___|
	// "Hey, I have this user ID, who is it?"
	// "Lemme figure it out"
	async loadUser(id) {
		if (app.users[id]) return;

		app.users[id] = { name: "[Unknown User]" };
		let out = await galaxy.json(`/api/users/whois?id=${id}`);
		if (out.name) app.users[id].name = out.name;
	},

	//  _       _                        _
	// (_)_ __ | |_ ___ _ __ _ __   __ _| |
	// | | '_ \| __/ _ \ '__| '_ \ / _` | |
	// | | | | | ||  __/ |  | | | | (_| | |
	// |_|_| |_|\__\___|_|  |_| |_|\__,_|_|
	logout(cookieGone) {
		localStorage.removeItem("user");
		if (!cookieGone) {
			galaxy.json("/api/users/logout");
			galaxy.alert.success("Logged out. Redirecting...");
		}

		setTimeout(() => {
			if (cookieGone) window.location.reload();
			else window.location.pathname = "";
		}, 1000);
	}
};

function createGalaxyAlert(elClass) {
	return msg => {
		const parent = document.createElement("div");
		parent.classList.add("alert", elClass);

		const text = document.createElement("span");
		text.textContent = msg;
		parent.appendChild(text);

		const close = document.createElement("span");
		close.classList.add("alert-close");
		close.textContent = "Close";
		close.onclick = function () {
			this.parentElement.remove();
		};
		parent.appendChild(close);

		document.body.appendChild(parent);
	};
}


console.log(`                     oooo
                     \`888
 .oooooooo  .oooo.    888   .oooo.   oooo    ooo oooo    ooo
888' \`88b  \`P  )88b   888  \`P  )88b   \`88b..8P'   \`88.  .8'
888   888   .oP"888   888   .oP"888     Y888'      \`88..8'
\`88bod8P'  d8(  888   888  d8(  888   .o8"'88b      \`888'
\`8oooooo.  \`Y888""8o o888o \`Y888""8o o88'   888o     .8'
d"     YD                                        .o..P'
"Y88888P'                                        \`Y8P'

Made w/ <3 by Yhvr`);