let comments = [];

const vue = {
	rate,
	publishComment,
	cvote,
	reply,
	attemptSendChat,


	replyingTo: 0,
	composingReply: "",
	composingComment: "",
	composingChat: "",
	updateVisible: false,

	data: {
		game: {},
		author: {},
		comments,
		chat: [],
		update: {},
	},
};

async function loadGame(id) {
	if (id === "") window.location.pathname = "";
	let game = await galaxy.json(`/api/games/info/?id=${id}`).catch(e => {
		window.location.pathname = "";
	});
	document.title = game.name
	app.data.game = game;
	app.data.game.id = id;

	console.log("game info", game);

	await galaxy.loadUser(game.author);
	app.data.author = app.users[game.author];
	// let author = await galaxy.json(`/api/users/whois?id=${game.author}`);
	// console.log("game author", author);
	// app.data.author = author;

	app.loading = false;

	let similarGames = await galaxy.json(`/api/games/similar?id=${id}`);
	console.log("similar games", similarGames)

	let commentRes = await galaxy.json(`/api/comments/fetch/new?id=${id}`);
	comments = commentRes;
	app.data.comments = comments;
	comments.forEach(async c => {
		await galaxy.loadUser(c.author);
	});

	let update = await galaxy.json(`/api/updates/get?id=${id}`);
	app.data.update = update;
}

async function rate(stars) {
	let newRating = await galaxy.post(`/api/rate/`, {
		id: gameId,
		rating: stars,
	});

	console.log(newRating);

	app.data.game.rating = newRating.rating;
	app.data.game.ratingCount = newRating.count;
}

async function publishComment() {
	// TODO show comment after submission
	let res = await galaxy.post(`/api/comments/new`, {
		game: gameId,
		content: app.composingComment,
	});

	if (res._resCode === 201) {
		app.data.comments.unshift(res.content)
		if (app.data.comments.length > 5) app.data.comments.pop();
		app.composingComment = "";
	}
}

async function cvote(id, vote) {
	galaxy.post(`/api/comments/vote`, { id, vote });

	// This is weird AF!
	let commentId;
	comments.forEach((comment, n) => {
		if (comment.id === id) commentId = n;
	});
	if (commentId === undefined) return;

	let comment = comments[commentId];
	// Why does this work?
	if (comment.yourVote === vote) {
		app.data.comments[commentId].score -= vote;
		app.data.comments[commentId].yourVote = 0;
	} else {
		app.data.comments[commentId].score -= comment.yourVote;
		app.data.comments[commentId].score += vote;
		app.data.comments[commentId].yourVote = vote;
	}
}

async function reply() {
	await galaxy.post(`/api/comments/reply`, { id: app.replyingTo, content: app.composingReply });

	let commentId;
	comments.forEach((comment, n) => {
		if (comment.id === app.replyingTo) commentId = n;
	});
	if (commentId === undefined) return;

	app.data.comments[commentId].devResponse = app.composingReply;
	app.replyingTo = 0;
	app.composingReply = "";
}

// Probably an awful way to do this LOL
let gameId = parseInt(window.location.pathname.split("/")[2]);
loadGame(gameId);

//       _           _
//   ___| |__   __ _| |_
//  / __| '_ \ / _` | __|
// | (__| | | | (_| | |_
//  \___|_| |_|\__,_|\__|
// And all the websocket fuckery that comes with it.
// TODO: switch from "ws" to "wss" once i get https set up
const ws = new WebSocket(`ws://${location.host}/`);
ws.addEventListener("message", async msg => {
	let content = JSON.parse(msg.data);

	switch (content.type) {
		case "getchannel":
			send({ type: "channel", value: parseInt(gameId) });
			break;
		case "nochannel":
			console.warn("No chat channel has been found :(");
			break;
		case "ready":
			app.data.chat.push("Chat connected!");
			break;
		case "message":
			// Lol this is probably really shitty
			// await galaxy.loadUser(content.author);
			app.data.chat.push(
				`${content.author}: ${content.value}`
			);
			break;
		case "alert":
			galaxy.alert[content.notif](content.value)
			break;
		default:
			console.warn("Unknown packet", content);
			break;
	}
});

function send(msg) {
	ws.send(JSON.stringify(msg));
}

let nextMessageSend = 0;
function sendChat() {
	if (nextMessageSend > Date.now()) return;
	send({ type: "message", value: app.composingChat });
	app.composingChat = "";

	nextMessageSend = Date.now() + 1000;
}

function attemptSendChat(e) {
	if (e.key === "Enter") sendChat();
}
