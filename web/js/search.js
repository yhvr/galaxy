const vue = {
	query: {
		term: "",
		tags: [],
		// "rating" (sortRating),
		// "update" (lastUpdate),
		sort: "",
		reverseSort: false,
	},

	games: [],

	search,
};

async function search() {
	let res = await galaxy.json(
		`/api/games/search?term=${
			app.query.term
		}&tags=${app.query.tags.join()}&sort=${app.query.sort}&reverseSort=${
			app.query.reverseSort
		}`
	);

	console.log(res);

	app.games = res;
}
