// Probably an awful way to do this LOL
let userId = window.location.pathname.split("/")[2];
loadUser(userId);

async function loadUser(id) {
	let { user, games, message } = await galaxy.json(`/api/users/whois/full?id=${id}`);

	app.data = { user, games }

	document.title = `${user.name} - galaxy`;

	app.menu = "loaded"
}