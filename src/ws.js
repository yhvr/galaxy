const { standalone } = require("../middleware/auth");
const { Server } = require("ws");
// TODO figure out how to get cookie-parser to parse the cookies
// that go thru the ws upgrade. so i don't need to use a separate
// lib here
const { parse } = require("cookie");
const { Game } = require("../models/game.model");
const { User } = require("../models/user.model");
const { Webhook } = require("discord-webhook-node");

// "oh my god why is everything stored in js! use mongo! you already have it set u-"
// the data is cleared on reboot and its easier to work with in js
const wsChannels = {};

const wss = new Server({ noServer: true });
wss.on("connection", async (ws, req) => {
	const cookies = parse(req.headers.cookie ?? "");
	let user = await standalone(cookies.token);

	user ||= { id: Math.random(), isAnon: true }

	let prev = getWs(user.id);
	if (prev) prev.close();

	ws.id = user.id;
	ws.name = user.name;
	ws.isAnon = !!user.isAnon;
	ws.last = 0;
	// nvm
	// // Alright, here's why this exists: So you can't
	// // be in multiple chat rooms at once. If you make
	// // a connection to room A, and then make another
	// // connection to room B, it'll kick you out of
	// // room A. so this is used in `ws.on("close")`
	// // (see a little bit below)
	send(ws, { type: "getchannel" });

	ws.on("message", async content => {
		let { data, error } = safeParse(content);

		if (error || !data.type) return;

		switch (data.type) {
			case "channel":
				// Init connection
				let game = await Game.findOne({ id: data.value });
				if (!game) return send(ws, { type: "nochannel" });
				if (!game.chatEnabled) return send(ws, { type: "nochannel" });
				// TODO probably flip off chatEnabled if game is private
				if (game.private) return send(ws, { type: "nochannel" });

				if (!wsChannels[game.id]) wsChannels[game.id] = {
					clients: [ws.id],
					webhook: new Webhook({ url: game.webhookURL, throwErrors: false, retryOnLimit: true }),
				};
				else wsChannels[game.id].clients.push(ws.id);

				ws.channel = game.id;
				send(ws, { type: "ready" });

				break;
			case "message":
				if (
					!data.value ||
					typeof data.value !== "string" ||
					data.value.length > 2000 ||
					ws.isAnon
				)
					return;

				console.log(data.value)

				if (ws.last > Date.now() - 1000) {
					send(ws, { type: "alert", notif: "error", value: "You can only send 1 message per second" });
					return;
				}
				ws.last = Date.now();

				let content = data.value.replace(/\n/giu, " ").trim();

				if (ws.channel) {
					sendBulk(wsChannels[ws.channel].clients, {
						type: "message",
						author: ws.name,
						value: content,
					});

					wsChannels[ws.channel].webhook.setUsername(ws.name);
					// TODO once a domain is obtained
					// wsChannels[ws.channel].webhook.setAvatar();

					// This looks stupid as fuck but it convinces the webhook
					// library to send the message without letting it mention
					// anybody
					wsChannels[ws.channel].webhook.send({
						getJSON() { return {
							content,
							allowed_mentions: { parse: [] }
						} }
					});
				}
				break;
		}
	});

	ws.on("close", () => {
		if (ws.channel) {
			wsChannels[ws.channel].clients = wsChannels[ws.channel].clients.filter(
				id => id !== ws.id
			);
		}
	});
});

function send(ws, obj) {
	if (typeof ws === "number") ws = getWs(ws);
	if (ws) ws.send(JSON.stringify(obj));
}

function sendBulk(socketIds, obj) {
	// TODO optimize so it only loops over all the connections once
	// would probably be more efficient in scenarios with 100s/1000s of clients
	socketIds.forEach(id => send(id, obj));
}

function getWs(id) {
	let out = false;
	wss.clients.forEach(ws => {
		if (ws.id === id) out = ws;
	});
	return out;
}

function safeParse(json) {
	let error = true;
	let data = {};

	try {
		data = JSON.parse(json);
		error = false;
	} catch (e) {}

	return { error, data };
}

module.exports = wss;
