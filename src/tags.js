const { readFileSync } = require("fs");

let allTags = readFileSync("config/tags.txt").toString().split("\n");

function validTagList(tags) {
	return Array.isArray(tags) && tags.length <= 3 && allValidTags(tags)
}

function allValidTags(tags) {
	return tags.every(tag => allTags.includes(tag));
}

function oneValidTag(tag) {
	return allTags.includes(tag);
}

function getAllTags() {
	return allTags;
}

function reloadTags() {
	allTags = readFileSync("config/tags.txt").toString().split("\n");
}

module.exports = {
	validTagList,
	allValidTags,
	oneValidTag,
	getAllTags,
	reloadTags,
};
