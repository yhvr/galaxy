# contributing

hey! thanks for your interest in contributing.

if you can't figure out what you want to add, join [my discord](https://yhvr.me/ego) and check the to-do list pinned in `#galaxy`.

that's about it, i think. don't stress too much on making it look pretty, the site is gonna be refactored before it releases ;)



oh, uh, code style? just use tabs, i guess